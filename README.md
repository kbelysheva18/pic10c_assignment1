This is the Assignment 1 repository.

This project creates a personalized vector<ItemType> class.
This program should run for ItemTypes of int, double, or std::string.

Gitignore:
I created a gitignore file to exclude binary and temporary files from the repository because git does not show the changes made in these files, and every build creates a new copy of the binary files.

Conflict resolution:
I encountered an issue with conflict resolution when I tried to push a change from the local repository while I had also changed something in the remote repository.
I had a conflict because I made a change to the readme file on the remote repository while also making changes in the local repository. 
I solved the issue by pulling changes to the readme file from the remote repository, and then merging everything and committing it from my local repository.
I learned when you call the pull command git automatically brings commits from the remote repository into the local repository. 

Test Cases:

 1) Create & display empty vector (v1)
    
    vector<int> v1;
    
    print_vector(v1);
    
    v1.dump_data();
	
 2) Populate & display vector with a certain amount of entries
   
   for ( int i = 1 ; i <= size ; ++i )
       
       v1.push_back(i);
   
   print_vector(v1);

 3) Copy non-empty vector, pop back last entry & display (v2)
    
    vector<int> v2(v1);
    
    v2.pop_back();
   
    print_vector(v2);

 4) Reassign vector (v1 = v2) & display
   
   v1 = v2;
   
   print_vector(v1);

 5) Dump contents of vectors (v1,v2)
   
   v1.dump_data();
    
   v2.dump_data();
